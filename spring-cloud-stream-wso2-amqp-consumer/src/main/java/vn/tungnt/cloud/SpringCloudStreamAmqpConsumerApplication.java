package vn.tungnt.cloud;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;

import vn.tungnt.cloud.domain.ImportingBatch;

@SpringBootApplication
@EnableBinding(Sink.class)
public class SpringCloudStreamAmqpConsumerApplication {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpringCloudStreamAmqpConsumerApplication.class);
	
	public static void main(String[] args) {
		SpringApplication.run(SpringCloudStreamAmqpConsumerApplication.class, args);
	}
	
	@StreamListener(target=Sink.INPUT)
	public void processBatch(ImportingBatch batch) {
		LOGGER.info("Received batch {}", batch.getBatchId());
	}
}
