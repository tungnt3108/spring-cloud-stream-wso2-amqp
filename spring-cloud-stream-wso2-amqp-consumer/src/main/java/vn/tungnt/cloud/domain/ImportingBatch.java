package vn.tungnt.cloud.domain;

public class ImportingBatch {
	
	private String batchId;
	
	private int document;

	public String getBatchId() {
		return batchId;
	}

	public ImportingBatch setBatchId(String batchId) {
		this.batchId = batchId;
		return this;
	}

	public int getDocument() {
		return document;
	}

	public ImportingBatch setDocument(int document) {
		this.document = document;
		return this;
	}
	
}
