package vn.tungnt.cloud;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.annotation.Poller;

import vn.tungnt.cloud.domain.ImportingBatch;

@SpringBootApplication
@EnableBinding(Source.class)
public class SpringCloudStreamAmqpProducerApplication {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SpringCloudStreamAmqpProducerApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudStreamAmqpProducerApplication.class, args);
	}
	
	@InboundChannelAdapter(channel=Source.OUTPUT, poller=@Poller(fixedDelay="1000"))
	public ImportingBatch importData() {
		String batchId = UUID.randomUUID().toString();
		LOGGER.info("::: Initialize Data {} :::", batchId);
		return new ImportingBatch()
				.setBatchId(batchId);
	}
}
