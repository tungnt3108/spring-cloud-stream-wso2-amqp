package vn.tungnt.cloud.infrastructure.channel;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface Wso2SourceQueueChannel {
	
	String EXCHANGE = "amq.direct";
	
	@Output(EXCHANGE)
	MessageChannel output();
}
